import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent  {

  nombreLower:string ='fernando';
  nommbreUpper: string = 'FERNANDO';
  nombreCompleto:string = 'FeRnaNdO HeRRerA';

  fecha: Date = new Date();

}
