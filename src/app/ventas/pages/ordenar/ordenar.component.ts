import { Component, OnInit } from '@angular/core';
import { Color, Heroe } from '../../interfaces/ventas.interfaces';

@Component({
  selector: 'app-ordenar',
  templateUrl: './ordenar.component.html',
  styles: [
  ]
})
export class OrdenarComponent  {
ordenarPor: string='';
enMayusculas: boolean=true;
heroes:Heroe[]=[
  {
    nombre:'A',
    vuela:false,
    color: Color.azul
  },
  {
    nombre:'Prueba',
    vuela:true,
    color: Color.azul
  },
  {
    nombre:'Prueba2',
    vuela:true,
    color:Color.rojo
  },
  {
    nombre:'Prueba3',
    vuela:false,
    color:Color.negro
  },
  {
    nombre:'A1',
    vuela:false,
    color: Color.azul
  }
]

toggleMayuscula(){
  this.enMayusculas=!this.enMayusculas;
}
cambiarOrden(valor:string){
  this.ordenarPor=valor;
}

}
