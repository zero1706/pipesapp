import { Component } from '@angular/core';
import { RESOURCE_CACHE_PROVIDER } from '@angular/platform-browser-dynamic';
import { interval }from 'rxjs';
@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html',
  styles: [
  ]
})
export class NoComunesComponent  {

  nombre :string='Susana';
  genero:string='femenino';

  invitacionMapa={
    'masculino':'invitarlo',
    'femenino':'invitarla'
  }

  clientes:string []=['kbrera','salinez','wayna','chuma0'];
  clientesMpa={
    '=0':'no hay clientes ps loco',
    '=1':'tenemos solo un cliente',
    'other':'tenemos # clientes esperando'
  }
  persona={
    nombre:'Zero',
    edad:21,
    direccion:'Ottawa,Canadá'
  }
  heroes=[{
    nombre:'Zero',
    edad:21,
    direccion:'Ottawa,Canadá'
  },
  {
    nombre:'Zero one',
    edad:22,
    direccion:'Ottawa,Canadá'
  },
  {
    nombre:'Zero two',
  edad:23,
  direccion:'Ottawa,Canadá'}
  ]
  cambiarPersona(){
    this.nombre='Melissa';
    this.genero='femenino'
  }
  borrarCliente(){
    this.clientes.pop();
  }

  myObservable = interval(5000);

  valorPromesa = new Promise ((resolve,reject) =>{
    setTimeout(()=>{
    resolve ('Tenemos data de un promesa');
    },5000)
  })
}
